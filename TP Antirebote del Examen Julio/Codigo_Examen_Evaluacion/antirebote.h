#ifndef ANTIREBOTE_H
#define ANTIREBOTE_H

enum EstadoBtnAntiRebote {
  Esperando,    // Esperar a que se aprete
  Confirmando,  // Confirmación si no es ruido
  Liberando,    // Esperar a que se suelte el boton
};

class BtnAntiRebote {
private:
  uint8_t pin_ = 0;
  EstadoBtnAntiRebote estado_ = EstadoBtnAntiRebote::Esperando;
  unsigned long ultimoCambio_ = 0;

  inline bool estadoApretado() {
    return invertirEntrada == false ? HIGH : LOW;
  }

  inline bool estadoSuelto() {
    return invertirEntrada == false ? LOW : HIGH;
  }

  inline void cambiarEstado(EstadoBtnAntiRebote nuevo, unsigned long ms) {
    estado_ = nuevo;
    ultimoCambio_ = ms;
  }

public:
  // Invierte la lectura del boton, sirve para cuando utilizas
  // INPUT_PULLUP o tenes botones donde HIGH == suelto
  bool invertirEntrada = false;
  bool valor = false;

  unsigned int tiempoRebote = 15;

  BtnAntiRebote(uint8_t pin, unsigned int _tiempoRebote = 15) {
    pinMode(pin, INPUT_PULLUP);
    pin_ = pin;
    tiempoRebote = _tiempoRebote;
  }

  BtnAntiRebote(uint8_t pin, bool _invertirEntrada, unsigned int tiempoRebote = 15) {
    BtnAntiRebote(pin, tiempoRebote);
    invertirEntrada = _invertirEntrada;
  }

  void update(unsigned long ms) {
    switch (estado_) {
      case Esperando:
        if (digitalRead(pin_) == estadoApretado()) {
          cambiarEstado(EstadoBtnAntiRebote::Confirmando, ms);
        }
        valor = false;
        break;
      case Confirmando:
        if (ms - ultimoCambio_ >= tiempoRebote) {
          if (digitalRead(pin_) == estadoSuelto()) {
            cambiarEstado(EstadoBtnAntiRebote::Esperando, ms);
          } else {
            cambiarEstado(EstadoBtnAntiRebote::Liberando, ms);
          }
        }
        break;
      case Liberando:
        if (digitalRead(pin_) == estadoSuelto()) {
          cambiarEstado(EstadoBtnAntiRebote::Confirmando, ms);
        }
        valor = true;
        break;
    }
  }
};

constexpr uint8_t sizeParaAgregar = 5;
BtnAntiRebote** botones = (BtnAntiRebote**)malloc(sizeParaAgregar * sizeof(BtnAntiRebote*));
size_t nBotones = 0;
size_t sizeBotonesArr = sizeParaAgregar;

size_t agregarBoton(uint8_t pin, unsigned int tiempoRebote = 15) {
  BtnAntiRebote* btn = new BtnAntiRebote(pin, tiempoRebote);
  nBotones++;
  // Necesitamos un array mas grande
  if (nBotones > sizeBotonesArr) {
    sizeBotonesArr += sizeParaAgregar;
    BtnAntiRebote** botones_realloc = (BtnAntiRebote**)realloc(botones, sizeBotonesArr * sizeof(BtnAntiRebote*));
    if (botones_realloc == NULL) {
      return false;
    } else {
      botones = botones_realloc;
    }
  }
  botones[nBotones - 1] = btn;
  return nBotones - 1;
}

size_t agregarBoton(uint8_t pin, bool invertirEntrada, unsigned int tiempoRebote = 15) {
  size_t salida = agregarBoton(pin, tiempoRebote);
  botones[nBotones - 1]->invertirEntrada = invertirEntrada;
  return salida;
}

void actualizarTodosLosBotones(unsigned long ms) {
  for (size_t i = 0; i < nBotones; i++) {
    botones[i]->update(ms);
  }
}

#endif