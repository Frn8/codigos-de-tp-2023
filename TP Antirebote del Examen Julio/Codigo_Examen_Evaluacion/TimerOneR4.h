#ifndef TimerOneR4_h_
#define TimerOneR4_h_

#include "FspTimer.h"
#include <Arduino.h>


uint64_t count = 0;

class TimerOne {
public:
  static void isrDefaultUnused(){};
  static void (*isrCallback)();
  static void isrFunc(timer_callback_args_t __attribute((unused)) * p_args) {
    Serial.println(count++);
    isrCallback();
  }

  void initialize(unsigned long microseconds = 1000000) __attribute__((always_inline)) {
    uint8_t timerType = GPT_TIMER;
    int8_t tIndex = FspTimer::get_available_timer(timerType);

    if (tIndex < 0) {
      tIndex = FspTimer::get_available_timer(timerType, true);
    }
    if (tIndex < 0) {
      for (;;) {
        Serial.println("Couldn't get available timer");
      }
    }

    FspTimer::force_use_of_pwm_reserved_timer();
    if (!_timer.begin(TIMER_MODE_PERIODIC, timerType, tIndex, microseconds, 0, isrFunc)) {
      for (;;) {
        Serial.println("Couldn't begin");
      }
    } else if (!_timer.setup_overflow_irq()) {
      for (;;) {
        Serial.println("Couldn't setup overflow irq");
      }
    } else if (!_timer.open()) {
      for (;;) {
        Serial.println("Not open");
      }
    } else if (!_timer.start()) {
      for (;;) {
        Serial.println("Coudln't start");
      }
    }
  }
  void setPeriod(unsigned long microseconds) __attribute__((always_inline)) {
    _timer.set_period_us(microseconds);
  }
  void start() __attribute__((always_inline)) {
    _timer.start();
  }
  void stop() __attribute__((always_inline)) {
    _timer.stop();
    _timer.reset();
  }
  void restart() __attribute__((always_inline)) {
    _timer.reset();
    _timer.start();
  }
  void resume() __attribute__((always_inline)) {
    _timer.start();
  }

  void attachInterrupt(void (*isr)()) __attribute__((always_inline)) {
    noInterrupts();
    isrCallback = isr;
    interrupts();
  }

  void attachInterrupt(void (*isr)(), unsigned long microseconds) __attribute__((always_inline)) {
    if (microseconds > 0) setPeriod(microseconds);
    attachInterrupt(isr);
  }
  void detachInterrupt() __attribute__((always_inline)) {
    isrCallback = isrDefaultUnused;
  }

private:
  FspTimer _timer;
};

void (*TimerOne::isrCallback)() = isrDefaultUnused;

#endif