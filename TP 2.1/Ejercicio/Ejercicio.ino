#define BTN_BAJO 8
#define BTN_ALTO 7

#define OUT_BOMBA 9
#define LED_LLENO 13
#define LED_VACIO 12

#define COMIENZO "*"
#define SEPARADOR "-"
#define FINAL "#"

enum EstadoBomba {
  Vacia,
  Intermedio,
  Llena,
  Error,
};

EstadoBomba estadoBomba;

unsigned short potenciaLlenado = 0;

void setupLedBomba() {
  pinMode(OUT_BOMBA, OUTPUT);
}

void loopLedBomba() {
  analogWrite(OUT_BOMBA, map(potenciaLlenado, 0, 100, 0, 255));
}

void setupSensores() {
  pinMode(BTN_BAJO, INPUT_PULLUP);
  pinMode(BTN_ALTO, INPUT_PULLUP);
  pinMode(LED_VACIO, OUTPUT);
  pinMode(LED_LLENO, OUTPUT);
}

void loopSensores() {
  digitalWrite(LED_VACIO, LOW);
  digitalWrite(LED_LLENO, LOW);
  if (digitalRead(BTN_BAJO) == LOW) {
    if (digitalRead(BTN_ALTO) == LOW) {
      digitalWrite(LED_LLENO, HIGH);
      estadoBomba = EstadoBomba::Llena;
      potenciaLlenado = 0;
    } else {
      estadoBomba = EstadoBomba::Intermedio;
    }
  } else {
    if (digitalRead(BTN_ALTO) == LOW) {
      digitalWrite(LED_LLENO, HIGH);
      digitalWrite(LED_VACIO, HIGH);
      estadoBomba = EstadoBomba::Error;
      // potenciaLlenado = 0;
    } else {
      digitalWrite(LED_VACIO, HIGH);
      estadoBomba = EstadoBomba::Vacia;
    }
  }
}

EstadoBomba ultimoEstadoBomba = EstadoBomba::Error;

void setupSerial() {
  Serial.begin(115200);
  Serial.setTimeout(100);

  while (!Serial)  // Esperar Serial
  {
  }
}

void loopSerial() {
  if (estadoBomba != ultimoEstadoBomba) {
    switch (estadoBomba) {
      case Llena:
        Serial.println("Carga Completa");
        break;
      case Vacia  :
        Serial.println("Tanque vacio");
        break;
      case Error:
        // Serial.println("Se detecto una inconsistencia en los sensores");
        break;
      default:
        break;
    }
  }

  if (Serial.available() > 0) {
    String texto = Serial.readStringUntil('\n');
    texto.trim();
    Serial.println(texto);
    if (texto.startsWith(COMIENZO) == false) {
      Serial.print("ERROR: El comando no empieza con ");
      Serial.println(COMIENZO);
      return;
    } else if (texto.endsWith(FINAL) == false) {
      Serial.print("ERROR: El comando no termina con ");
      Serial.println(FINAL);
      return;
    } else if (texto.indexOf(SEPARADOR, texto.indexOf(SEPARADOR) + 1) != -1) { // Ver si hay mas de un separador
      Serial.print("ERROR: El comando tiene más de un ");
      Serial.println(SEPARADOR);
      return;
    }

    String nombre = texto.substring(sizeof(COMIENZO) - 1, texto.indexOf(SEPARADOR));
    nombre.trim();

    if (nombre.equalsIgnoreCase("c") == true) {
      String textoPorcentaje = texto.substring(texto.indexOf(SEPARADOR) + 1);
      int porcentaje = textoPorcentaje.toInt();
      if (porcentaje < 0) {
        Serial.println("ERROR: El argumento no puede ser menor a 0");
        return;
      }
      if (porcentaje > 100) {
        Serial.println("ERROR: El argumento no puede ser mayor a 100");
        return;
      }
      Serial.print("Llenando ");
      Serial.print(porcentaje);
      Serial.println("%");
      potenciaLlenado = porcentaje;
    } else {
      Serial.println("ERROR: El comando no existe");
      return;
    }
  }

  ultimoEstadoBomba = estadoBomba;
}

void setup() {
  setupSensores();
  setupLedBomba();
  setupSerial();
}

void loop() {
  loopSensores();
  loopLedBomba();
  loopSerial();
}
