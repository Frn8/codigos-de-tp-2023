class UserLogin {
protected:
  String email;
  String password;
public:
  UserLogin() {}

  UserLogin(String email, String password)
    : email(email), password(password) {}

  bool checkInfo(String inputEmail, String inputPassword) {
    return inputEmail == email && inputPassword == password;
  }
};

UserLogin users[] = {
  UserLogin("email@e.com", "ABC123")
};

bool checkUserPassword(String email, String password) {
  for (int i = 0; i < sizeof(users) / sizeof(UserLogin); i++) {
    UserLogin user = users[i];
    if (user.checkInfo(email, password)) {
      return true;
    }
  }
  return false;
}

void setup() {
  Serial.begin(115200);
}

void loop() {
  Serial.print("Email: ");
  while (Serial.available() == 0) {}
  String email = Serial.readStringUntil('\n');
  email.trim();
  Serial.println(email);
  Serial.print("Contraseña: ");
  while (Serial.available() == 0) {}
  String password = Serial.readStringUntil('\n');
  password.trim();
  Serial.println("***Contraseña recivida***");
  if (checkUserPassword(email, password)) {
    Serial.print("Bienvenido, ");
    Serial.println(email);
  } else {
    Serial.println("Email o contraseña incorrecto o inexistente");
  }
}
