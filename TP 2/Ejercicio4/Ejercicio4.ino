#define ROJO 11
#define VERDE 9
#define AZUL 10

#define COMIENZO '*'
#define SEPARADOR ','
#define FINAL '#'
#define NUMERO_VALORES 3

void setup() {
  Serial.begin(115200);
  Serial.setTimeout(150);
  pinMode(ROJO, OUTPUT);
  pinMode(VERDE, OUTPUT);
  pinMode(AZUL, OUTPUT);
}

struct Resultado {
  uint8_t r;
  uint8_t g;
  uint8_t b;
};

struct Resultado leerDatos(String entrada) {
  uint8_t rojo = 0;
  uint8_t verde = 0;
  uint8_t azul = 0;
  if (entrada.startsWith(String(COMIENZO)) == false) {
    return Resultado {};
  }
  if (entrada.endsWith(String(FINAL)) == false) {
    return Resultado {};
  }

  String valores = entrada.substring(1, entrada.length() - 1);
  // Serial.println(valores);

  int indexComas[NUMERO_VALORES - 1];
  int ultimoIndex = 0;

  // Buscamos las comas en el texto
  for (int i = 0; i < NUMERO_VALORES - 1; i++) {
    int encontrado = valores.indexOf(SEPARADOR, ultimoIndex + 1);
    // Serial.println(encontrado);
    if (encontrado == -1) {
      break;
    }
    indexComas[i] = encontrado + 1;
    ultimoIndex = encontrado;
  }

  // Convertimos los valores a ints
  rojo = valores.substring(0, indexComas[0]).toInt();
  verde = valores.substring(indexComas[0], indexComas[1]).toInt();
  azul = valores.substring(indexComas[1]).toInt();

  return Resultado {rojo, verde, azul};
}

void esperarTexto() {
  while (Serial.available() == 0) {
    
  }
}

void loop() {
  esperarTexto();
  String entrada = Serial.readStringUntil('\n');
  auto [rojo, verde, azul] = leerDatos(entrada);
  Serial.println("Lectura: ");
  Serial.print(rojo);
  Serial.print(",");
  Serial.print(verde);
  Serial.print(",");
  Serial.println(azul);
  Serial.println("---------------------------");
  analogWrite(ROJO, map(rojo, 0, 100, 0, 255));
  analogWrite(VERDE, map(verde, 0, 100, 0, 255));
  analogWrite(AZUL, map(azul, 0, 100, 0, 255));
}
