float potenciaT(float base, int exponente) {
  float resultado = 1.0;
  for (int i = 0; i < exponente; i++) {
    resultado = resultado * base;
  }
  return resultado;
}

float potencia(float base, int exponente) {
  if (exponente == 0) {
    return 1;
  } else if (exponente < 0) {
    return potenciaT(1.0 / base, -exponente);
  }
  return potenciaT(base, exponente);
}

void setup() {
  Serial.begin(115200);
  Serial.setTimeout(150);
}

void loop() {
  Serial.print("Base: ");
  while (Serial.available() == 0) {}
  float base = Serial.parseFloat();
  Serial.println(base);
  Serial.print("Exponente: ");
  while (Serial.available() == 0) {}
  long exponente = Serial.parseInt();
  Serial.println(exponente);
  Serial.print("Resultado: ");
  float resultado = potencia(base, exponente);
  Serial.println(resultado);
}
