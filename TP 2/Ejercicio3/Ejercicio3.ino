// Necesaria para mantener el estado de los pines en digitalToggle
bool pinsState[NUM_DIGITAL_PINS];

void digitalToggle(int pin) {
  pinsState[pin] = !pinsState[pin];
  digitalWrite(pin, pinsState[pin]);
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  digitalToggle(LED_BUILTIN);
  delay(500);
}
