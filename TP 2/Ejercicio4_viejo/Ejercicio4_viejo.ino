#define ROJO 11
#define VERDE 10
#define AZUL 9

#define COMIENZO '*'
#define SEPARADOR ','
#define FINAL '#'

enum EstadoLecturaColor {
  Esperando,
  LeyendoRojo,
  LeyendoVerde,
  LeyendoAzul
};

void setup() {
  Serial.begin(115200);
  pinMode(ROJO, OUTPUT);
  pinMode(VERDE, OUTPUT);
  pinMode(AZUL, OUTPUT);
}

uint8_t* leerDatos() {
  EstadoLecturaColor estadoLecturaColor = EstadoLecturaColor::Esperando;
  bool listo = false;
  uint8_t rojo = 0;
  uint8_t verde = 0;
  uint8_t azul = 0;
  while (!listo) {
    while (Serial.available() > 0) {
      switch (estadoLecturaColor) {
        case Esperando:
          if (Serial.read() == COMIENZO) {
            estadoLecturaColor = EstadoLecturaColor::LeyendoRojo;
          }
          break;
        case LeyendoRojo:
          rojo = Serial.parseInt();
          if (Serial.read() == SEPARADOR) {
            estadoLecturaColor = EstadoLecturaColor::LeyendoVerde;
          }
          break;
        case LeyendoVerde:
          verde = Serial.parseInt();
          if (Serial.read() == SEPARADOR) {
            estadoLecturaColor = EstadoLecturaColor::LeyendoAzul;
          }
          break;
        case LeyendoAzul:
          azul = Serial.parseInt();
          if (Serial.read() == FINAL) {
            estadoLecturaColor = EstadoLecturaColor::Esperando;
            listo = true;
          }
          break;
      }
    }

  }
  static uint8_t resultado[3];
  resultado[0] = rojo;
  resultado[1] = verde;
  resultado[2] = azul;
  return resultado;
}

void loop() {
  uint8_t* datos = leerDatos();
  uint8_t rojo = map(datos[0], 0, 100, 0, 255);
  uint8_t verde = map(datos[1], 0, 100, 0, 255);
  uint8_t azul = map(datos[2], 0, 100, 0, 255);
  analogWrite(ROJO, datos[0]);
  analogWrite(VERDE, datos[1]);
  analogWrite(AZUL, datos[2]);
}
