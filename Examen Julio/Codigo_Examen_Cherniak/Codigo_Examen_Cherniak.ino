// Includes

#if AVR
#include <TimerOne.h>
#endif

// Inputs

#define SENSOR_HUMEDAD A0
#define BOT_1 6
#define SWT_1 7

// Outputs

#define LED_INDICADOR 3
#define LED_KEEP_ALIVE 2
#define BOMBA_AGUA 4

// Config

#if AVR
#define USAR_TIMER1
#endif

constexpr unsigned long TIEMPO_BLINK_KEEP_ALIVE = 500;

#ifdef TARGET_RP2040
constexpr uint16_t MV_MICRO = 3333;
constexpr uint16_t MAX_ADC = 4095;
#else
constexpr uint16_t MV_MICRO = 5000;
constexpr uint16_t MAX_ADC = 1023;
#endif

constexpr uint8_t DIGITOS_1 = 14;
constexpr uint8_t DIGITOS_2 = 87;
constexpr auto TIEMPO_1 = ((DIGITOS_1 + 10) / 10) * 1000;
constexpr auto TIEMPO_2 = ((DIGITOS_2 + 10) / 10) * 1000;

// Globales

unsigned long ms = 0;

uint16_t mvHumedad = 0;

// Timer interrupts

#ifdef USAR_TIMER1
void contarMs()
{
  ms++;
}
#endif

// El resto del código

enum EstadoLed
{
  OffLed = LOW,
  OnLed = HIGH,
};

void ledKeepAliveBlink()
{
  static unsigned long ultimoCambio = ms;
  static EstadoLed estadoLed = EstadoLed::OnLed;
  switch (estadoLed)
  {
  case OnLed:
    digitalWrite(LED_KEEP_ALIVE, HIGH);
    if (ms - ultimoCambio >= TIEMPO_BLINK_KEEP_ALIVE)
    {
      estadoLed = EstadoLed::OffLed;
      ultimoCambio = ms;
    }
    break;
  case OffLed:
    digitalWrite(LED_KEEP_ALIVE, LOW);
    if (ms - ultimoCambio >= TIEMPO_BLINK_KEEP_ALIVE)
    {
      estadoLed = EstadoLed::OnLed;
      ultimoCambio = ms;
    }
    break;
  }
}

inline uint16_t leerHumedad()
{
  return analogRead(SENSOR_HUMEDAD);
}

inline uint16_t humedadRaw2mV(uint16_t raw)
{
  return map(raw, 0, MAX_ADC, 0, MV_MICRO);
}

void loopHumedad()
{
  uint16_t raw = leerHumedad();
  mvHumedad = humedadRaw2mV(raw);
}

enum SeleccionModo
{
  Modo1 = 1,
  Modo2
} modo;

void cambiarModo()
{
  if (digitalRead(SWT_1) == LOW)
  {
    modo = SeleccionModo::Modo1;
  }
  else
  {
    modo = SeleccionModo::Modo2;
  }
}

enum EstadoBomba
{
  OffBomba,
  OnBomba,
  PausaBomba,
  EsperandoSoltarBoton,
  EsperandoPresionarBoton,
};

void modo1()
{
  digitalWrite(LED_INDICADOR, HIGH);
  static EstadoBomba estado = EstadoBomba::OffBomba;
  static int contadorPulsaciones = 0; // Contador para las pulsaciones del botón
  int lecturaBoton = digitalRead(BOT_1);

  switch (estado)
  {
  case OffBomba:
    digitalWrite(BOMBA_AGUA, LOW);
    if (mvHumedad <= 2000)
    {
      estado = EstadoBomba::OnBomba;
    }
    if (lecturaBoton == HIGH)
    {
      estado = EstadoBomba::EsperandoSoltarBoton;
    }
    break;
  case OnBomba:
    digitalWrite(BOMBA_AGUA, HIGH);
    if (mvHumedad >= 2500)
    {
      estado = EstadoBomba::OffBomba;
    }
    if (lecturaBoton == HIGH)
    {
      estado = EstadoBomba::EsperandoSoltarBoton;
    }
    break;
  case PausaBomba:
    digitalWrite(BOMBA_AGUA, LOW);
    if (lecturaBoton == HIGH)
    {
      estado = EstadoBomba::EsperandoSoltarBoton;
    }
    break;
  case EsperandoSoltarBoton:
    if (lecturaBoton == LOW)
    {
      estado = EstadoBomba::EsperandoPresionarBoton;
    }
    break;
  case EsperandoPresionarBoton:
    if (lecturaBoton == HIGH)
    {
      contadorPulsaciones++;
      if (contadorPulsaciones >= 2)
      {
        estado = EstadoBomba::OffBomba;
        contadorPulsaciones = 0;
      }
      else
      {
        estado = EstadoBomba::EsperandoSoltarBoton;
      }
    }
    break;
  }
}

void modo2()
{
  digitalWrite(LED_INDICADOR, LOW);
  static EstadoBomba estado = EstadoBomba::OffBomba;
  static unsigned long ultimoCambio = ms;
  switch (estado)
  {
  case OffBomba:
    digitalWrite(BOMBA_AGUA, LOW);
    if (ms - ultimoCambio >= TIEMPO_2)
    {
      estado = EstadoBomba::OnBomba;
      ultimoCambio = ms;
    }
    break;
  case OnBomba:
    digitalWrite(BOMBA_AGUA, HIGH);
    if (ms - ultimoCambio >= TIEMPO_1)
    {
      estado = EstadoBomba::OffBomba;
      ultimoCambio = ms;
    }
    break;
  }
}

void setup()
{
  Serial.begin(115200);

#ifdef TARGET_RP2040
  analogReadResolution(12);
#endif

  pinMode(SENSOR_HUMEDAD, INPUT);
  pinMode(BOT_1, INPUT_PULLUP);
  pinMode(SWT_1, INPUT_PULLUP);

  pinMode(LED_INDICADOR, OUTPUT);
  pinMode(LED_KEEP_ALIVE, OUTPUT);
  pinMode(BOMBA_AGUA, OUTPUT);

#ifdef USAR_TIMER1
  Timer1.initialize(1000);
  Timer1.attachInterrupt(contarMs);
#endif
}

void loop()
{
#ifndef USAR_TIMER1
  ms = millis();
#endif
  Serial.println(mvHumedad);
  cambiarModo();
  loopHumedad();
  ledKeepAliveBlink();
  switch (modo)
  {
  case Modo1:
    modo1();
    break;
  case Modo2:
    modo2();
    break;
  }
}