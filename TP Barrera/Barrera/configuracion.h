#ifndef CONFIG_H
#define CONFIG_H
constexpr int16_t BARRERA_ANGULO_ABIERTA = 180;
constexpr int16_t BARRERA_ANGULO_CERRRADA = 20;
constexpr uint8_t BARRERA_SERVO_PIN = 11;

constexpr uint8_t SENSOR_ENTRADA_PIN = 8;
constexpr uint8_t SENSOR_SALIDA_PIN = 9;

constexpr uint8_t LED_PIN = 10;

constexpr char PASS[] = "abc";
#endif