#include <Servo.h>
#include <FastLED.h>

#include "configuracion.h"

// Consigna: Hay que hacer un sistema donde se recive una contraseña por serial
// se verifica y si es correcta, se abre la barrera cuando llega el auto al primer sensor
// Después se espera a que el auto pase completamente (detecte y deje de detectar).
// También hay que poner un neopixel que este rojo cuando esta cerrada y verde cuando este abierta

typedef enum {
  EsperandoPass,
  EsperandoAuto,
} est_Estados;

typedef enum {
  Llegue,
  Pase,
  Retire,
} est_EsperandoAuto;

class Barrera {
public:
  int16_t anguloAbierta = BARRERA_ANGULO_ABIERTA;
  int16_t anguloCerrada = BARRERA_ANGULO_CERRRADA;

  Barrera() {}

  Barrera(int pin) {
    _servo.attach(pin);
  }

  void abrir() {
    girar(anguloAbierta);
  }

  void cerrar() {
    girar(anguloCerrada);
  }

  void girar(int16_t angulo) {
    if (angulo >= -180 && angulo <= 180) {
      _servo.write(angulo);
    }
  }

private:
  Servo _servo;
  bool _abierta = false;
};

class SensorAuto {
public:
  SensorAuto(uint8_t pin) {
    _pin = pin;
    pinMode(_pin, INPUT_PULLUP);
  }

  SensorAuto() {}

  bool detectar() {
    return digitalRead(_pin) == LOW;
  }
private:
  uint8_t _pin;
};

CRGB leds[1];

class Controlador {
public:
  Controlador() {
  }

  Controlador(uint8_t pinBarrera, uint8_t pinSensorEntrada, uint8_t pinSensorSalida) {
    FastLED.show();
    _barrera = Barrera(pinBarrera);
    _barrera.cerrar();
    _sensorAfuera = SensorAuto(pinSensorEntrada);
    _sensorAdentro = SensorAuto(pinSensorSalida);
  }

  void actualizar() {
    // Serial.println(_estado);
    // Serial.println(_estadoAuto);
    FastLED.show();
    switch (_estado) {
      case EsperandoPass:
        {
          leds[0] = CRGB::Red;
          if (_sensorAdentro.detectar()) {
            _permitirPaso();
            break;
          }
          if (Serial.available() != 0) {
            String pass = Serial.readStringUntil('\n');
            pass.trim();
            if (pass == PASS) {
              _permitirPaso();
            } else {
              _bloquearPaso();
            }
            break;
          }
          break;
        }
      case EsperandoAuto:
        {
          static enum Sensor {
            Ninguno,
            Afuera,
            Adentro
          } primeraDeteccion;
          switch (_estadoAuto) {
            case Llegue:
              if (_sensorAfuera.detectar() || _sensorAdentro.detectar()) {
                if (_sensorAdentro.detectar()) {
                  primeraDeteccion = Sensor::Adentro;
                } else if (_sensorAfuera.detectar()) {
                  primeraDeteccion = Sensor::Afuera;
                } else {
                  _barrera.cerrar();
                  for (;;) {
                    Serial.println("?????? que como paso esto ????");
                  }
                }
                leds[0] = CRGB::Green;
                _estadoAuto = est_EsperandoAuto::Pase;
              }
              break;
            case Pase:
              _barrera.abrir();
              if (primeraDeteccion == Sensor::Adentro) {
                if (_sensorAdentro.detectar()) {
                  _estadoAuto = est_EsperandoAuto::Retire;
                }
              } else if (primeraDeteccion == Sensor::Afuera) {
                if (_sensorAfuera.detectar()) {
                  _estadoAuto = est_EsperandoAuto::Retire;
                }
              }
              break;
            case Retire:
              static bool ultimoEstado = false;
              static int cambios = 0;
              bool estado = false;
              if (primeraDeteccion == Sensor::Afuera) {
                estado = _sensorAdentro.detectar();
              } else if (primeraDeteccion == Sensor::Adentro) {
                estado = _sensorAfuera.detectar();
              } else {
                _barrera.cerrar();
                for (;;) {
                  Serial.println("Che, deja de jugar con el estado interno >:(");
                }
              }
              if (estado != ultimoEstado) {
                ultimoEstado = estado;
                cambios++;
              }
              if (cambios == 2) {
                _barrera.cerrar();
                primeraDeteccion = Sensor::Ninguno;
                _estadoAuto = est_EsperandoAuto::Llegue;
                _estado = est_Estados::EsperandoPass;
                cambios = 0;
              } else if (cambios >= 1) {
                leds[0] = CRGB::Orange;
              } else if (cambios > 2) {
                _barrera.cerrar();
                for (;;) {
                  Serial.println("Che, deja de jugar con el estado interno");
                }
              }
          }
          break;
        }
        break;
    }
  }



  inline est_Estados
  getEstado() {
    return _estado;
  }

private:
  void _permitirPaso() {
    Serial.println("Bienvenido");
    // _barrera.abrir();
    _estado = est_Estados::EsperandoAuto;
    _estadoAuto = est_EsperandoAuto::Llegue;
    leds[0] = CRGB::Orange;
  }

  void _bloquearPaso() {
    leds[0] = CRGB::Red;
    _barrera.cerrar();
    _estado = est_Estados::EsperandoPass;
    _estadoAuto = est_EsperandoAuto::Llegue;
    Serial.println("Contraseña Incorrecta");
  }

  est_Estados _estado = est_Estados::EsperandoPass;
  est_EsperandoAuto _estadoAuto = est_EsperandoAuto::Llegue;

  Barrera _barrera;

  SensorAuto _sensorAfuera;
  SensorAuto _sensorAdentro;
};

Controlador controlador1;

void setup() {
  delay(100);
  Serial.begin(115200);
  Serial.setTimeout(100);
  FastLED.addLeds<WS2812B, LED_PIN, GRB>(leds, 1);
  leds[0] = CRGB::Red;
  controlador1 = Controlador(BARRERA_SERVO_PIN, SENSOR_ENTRADA_PIN, SENSOR_SALIDA_PIN);
}

void loop() {
  controlador1.actualizar();
}
