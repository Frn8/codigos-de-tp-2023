void setup() {
  Serial.begin(115200);
}

void loop() {
  if (Serial.available() > 0) {
    char input = Serial.read();
    bool isSpecial = false;
    String specialText = "";
    switch (input) {
      case '\n':
        isSpecial = true;
        specialText = "(linea nueva)";
        break;
      case '\t':
        isSpecial = true;
        specialText = "(tab)";
        break;
      case '\r':
        isSpecial = true;
        specialText = "(retorno de carro)";
        break;
      case 0:
        isSpecial = true;
        specialText = "(nulo)";
        break;
      case ' ':
        isSpecial = true;
        specialText = "(espacio)";
        break;
    }
    Serial.print("El character ingresado fue ");
    if (isSpecial) {
      Serial.print(specialText);
    } else {
      Serial.print(input);
    }
    Serial.print(" y su codificación ASCII es ");
    Serial.println(input, DEC);
  }
}
