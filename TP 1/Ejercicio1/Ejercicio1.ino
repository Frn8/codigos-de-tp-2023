#include <limits.h>
#define MAX_NUMEROS 20

void setup() {
  Serial.begin(115200);
  Serial.setTimeout(60);
}

void loop() {
  long mayor = LONG_MIN;
  long menor = LONG_MAX;
  auto total = 0;

  unsigned int numerosLeidos = 0;

  while (numerosLeidos < MAX_NUMEROS) {
    if (Serial.available() > 0) {
      long input = Serial.parseInt();

      if (input > mayor) {
        mayor = input;
      }
      if (input < menor) {
        menor = input;
      }
      total += input;
      numerosLeidos++;
      Serial.println(input);
    }
  }

  Serial.println("-----------------------");
  Serial.print("Promedio: ");
  Serial.println((float)total / MAX_NUMEROS);
  Serial.print("Mayor: ");
  Serial.println(mayor);
  Serial.print("Menor: ");
  Serial.println(menor);
}
