#define VERDE 11
#define ROJO 10
#define AZUL 9

void setup() {
  Serial.begin(115200);
  pinMode(VERDE, OUTPUT);
  pinMode(ROJO, OUTPUT);
  pinMode(AZUL, OUTPUT);
}

void loop() {
  if (Serial.available() > 0) {
    char input = tolower(Serial.read());
    byte colorR = 0;
    byte colorG = 0;
    byte colorB = 0;
    switch (input) {
      case 'r':
        colorR = 0xff;
        colorG = 0x00;
        colorB = 0x00;
        break;
      case 'g':
        colorR = 0x00;
        colorG = 0xff;
        colorB = 0x00;
        break;
      case 'b':
        colorR = 0x00;
        colorG = 0x00;
        colorB = 0xff;
        break;
    }
    analogWrite(ROJO, colorR);
    analogWrite(VERDE, colorG);
    analogWrite(AZUL, colorB);
  }
}
