#define N_MESES 12

const String TITULO_MES = "Mes";
const String TITULO_INGRESO = "Ingreso";
const String TEXTO_MAYOR_MES = "**Mes de mayor ingreso**";
const String TEXTO_MENOR_MES = "**Mes de menor ingreso**";
const String TEXTO_PROMEDIO = "**Promedio**";
const String TEXTO_TOTAL = "**Total**";

const String MESES[] = {
  "Enero",
  "Febrero",
  "Marzo",
  "Abril",
  "Mayo",
  "Junio",
  "Julio",
  "Agosto",
  "Septiembre",
  "Octubre",
  "Nobiembre",
  "Diciembre"
};

// Convierte el numero a un mes en texto
inline String numeroAMes(unsigned int mes) {
  if (mes >= N_MESES) {
    return "Mes " + String(mes + 1);
  } else {
    return MESES[mes];
  }
}

// Convierte el numero a un mes en texto.
// Nota: primeraMayus solo funciona si no hay un nombre para ese mes.
inline String numeroAMes(unsigned int mes, bool primeraMayus) {
  if (mes >= N_MESES) {
    if (primeraMayus) {
      return "Mes " + String(mes + 1);
    } else {
      return "mes " + String(mes + 1);
    }
  } else {
    return MESES[mes];
  }
}

// No se, esto ya estaba en el de antes...
template<typename T>
T mayorMes(T valores[], size_t cantidadMeses) {
  if (cantidadMeses <= 0) {
    return -1;
  }
  T maxEncontrado = valores[0];
  unsigned int mesMasAlto = 0;
  for (int i = 1; i < cantidadMeses; i++) {
    if (valores[i] > maxEncontrado) {
      maxEncontrado = valores[i];
      mesMasAlto = i;
    }
  }
  return mesMasAlto;
}

// No se, esto ya estaba en el de antes...
template<typename T>
T menorMes(T valores[], size_t cantidadMeses) {
  if (cantidadMeses <= 0) {
    return -1;
  }
  T minEncontrado = valores[0];
  unsigned int mesMasBajo = 0;
  for (int i = 1; i < cantidadMeses; i++) {
    if (valores[i] < minEncontrado) {
      minEncontrado = valores[i];
      mesMasBajo = i;
    }
  }
  return mesMasBajo;
}

// Busca la mayor longitud de un array de Strings.
size_t maxStringLength(float ingresos[], size_t cantidadMeses, bool esMes, String extras[], size_t cantidadExtras) {
  size_t lengthMax = 0;
  for (unsigned int i = 0; i < cantidadMeses; i++) {
    size_t valorLength;
    if (esMes) {
      valorLength = numeroAMes(i).length();
    } else {
      valorLength = String("$" + String(ingresos[i], 2)).length();
    }
    if (valorLength > lengthMax) {
      lengthMax = valorLength;
    }
  }

  for (unsigned int i = 0; i < cantidadExtras; i++) {
    size_t valorLength = extras[i].length() + 1;
    if (valorLength > lengthMax) {
      lengthMax = valorLength;
    }
  }
  return lengthMax + 1;
}

float pedirIngreso(uint8_t mes) {
  Serial.print("Ingresos de");
  String textoMes = numeroAMes(mes, false);
  if (mes >= N_MESES) {
    Serial.print("l " + textoMes);
  } else {
    Serial.print(" " + textoMes);
  }
  Serial.print(": $");
  while (!Serial.available()) {
    delay(2);
  }
  String entrada = Serial.readStringUntil('\n');  // Esto es para sacar la linea nueva
  entrada.replace(',', '.');                      // Cambiar la , por el punto
  float valor = entrada.toFloat();
  Serial.println(entrada);
  return valor;
}

void printSeparador(size_t maxLongitud) {
  Serial.print("|");
  for (unsigned int i = 0; i < maxLongitud; i++) {
    Serial.print("-");
  }
}

void printCelda(String contenido, size_t maxLongitud) {
  Serial.print("|");
  Serial.print(contenido);
  if (maxLongitud <= contenido.length() - 1) {
    return;
  }
  for (unsigned int i = 0; i < maxLongitud - contenido.length(); i++) {
    Serial.print(" ");
  }
}

void printTablaIngresos(float ingresos[], size_t cantidad) {
  // Lo hago ahora porque si no se puede romper la tabla y su formato
  unsigned int mesMasAlto = mayorMes(ingresos, N_MESES);
  unsigned int mesMasBajo = menorMes(ingresos, N_MESES);
  String mayorMesTexto = MESES[mesMasAlto] + " ($" + String(ingresos[mesMasAlto], 2) + ")";
  String menorMesTexto = MESES[mesMasBajo] + " ($" + String(ingresos[mesMasBajo], 2) + ")";

  String extrasMes[] = { TITULO_MES, TEXTO_PROMEDIO, TEXTO_TOTAL, TEXTO_MAYOR_MES, TEXTO_MENOR_MES };
  size_t maxMesLongitud = maxStringLength(ingresos, N_MESES, true, extrasMes, 5);
  String extrasIngresos[] = { TITULO_INGRESO, mayorMesTexto, menorMesTexto };
  size_t maxIngresosLongitud = maxStringLength(ingresos, N_MESES, false, extrasIngresos, 3);

  printCelda(TITULO_MES, maxMesLongitud);
  printCelda(TITULO_INGRESO, maxIngresosLongitud);
  Serial.println("|");
  printSeparador(maxMesLongitud);
  printSeparador(maxIngresosLongitud);
  Serial.println("|");

  float total = 0;
  for (unsigned int i = 0; i < cantidad; i++) {
    total += ingresos[i];
    printCelda(numeroAMes(i), maxMesLongitud);
    printCelda("$" + String(ingresos[i], 2), maxIngresosLongitud);
    Serial.println("|");
  }

  printCelda(TEXTO_MAYOR_MES, maxMesLongitud);
  printCelda(mayorMesTexto, maxIngresosLongitud);
  Serial.println("|");
  
  printCelda(TEXTO_MENOR_MES, maxMesLongitud);
  printCelda(menorMesTexto, maxIngresosLongitud);
  Serial.println("|");

  printCelda(TEXTO_PROMEDIO, maxMesLongitud);
  printCelda("$" + String(total / cantidad, 2), maxIngresosLongitud);
  Serial.println("|");

  printCelda(TEXTO_TOTAL, maxMesLongitud);
  printCelda("$" + String(total, 2), maxIngresosLongitud);
  Serial.println("|");
}

void setup() {
  Serial.begin(115200);
  while (!Serial) {}
  delay(200);
  Serial.println("Hola!");
  delay(300);
}

void loop() {
  float ingresos[N_MESES];
  for (int i = 0; i < N_MESES; i++) {
    ingresos[i] = pedirIngreso(i);
  }
  Serial.println("Resumen:");
  printTablaIngresos(ingresos, N_MESES);
  delay(2000);
}
