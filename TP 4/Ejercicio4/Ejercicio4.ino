#define N_ELEMENTOS 12

template<typename T>
inline void quicksort(T arr[], size_t cantidad) {
  quicksort(arr, 0, cantidad - 1);
}

template<typename T>
inline void quicksort(T arr[], size_t inicio, size_t fin) {
  if (inicio < fin) {
    size_t piv = particion(arr, inicio, fin);
    quicksort(arr, inicio, piv - 1);
    quicksort(arr, piv + 1, fin);
  }
}

template<typename T>
inline size_t particion(T arr[], size_t bajo, size_t alto) {
  T piv = arr[alto];

  size_t i = bajo - 1;
  for (size_t j = bajo; j < alto; j++) {
    if (arr[j] <= piv) {
      i++;
      // Intercambio
      T acumulador = arr[j];
      arr[j] = arr[i];
      arr[i] = acumulador;
    }
  }

  i++;
  // Intercambio
  T acumulador = arr[alto];
  arr[alto] = arr[i];
  arr[i] = acumulador;
  return i;
}

void setup() {
  Serial.begin(115200);
  
  // Esperar por Serial en Micro, Leonardo, Pico(W), etc.
  while (!Serial) {}
}

void loop() {
  int32_t arr[N_ELEMENTOS];
  for (size_t i = 0; i < N_ELEMENTOS; i++) {
    Serial.print("Introduce el elemento ");
    Serial.print(i + 1);
    Serial.print(": ");
    while (Serial.available() == 0) {}
    String texto = Serial.readStringUntil('\n');
    arr[i] = texto.toInt();
    Serial.println(texto);
  }
  Serial.print("Numeros: ");
  for (size_t i = 0; i < N_ELEMENTOS; i++) {
    Serial.print(arr[i]);
    Serial.print(" ");
  }
  Serial.println();
  quicksort(arr, N_ELEMENTOS);
  Serial.print("Ordenado: ");
  for (size_t i = 0; i < N_ELEMENTOS; i++) {
    Serial.print(arr[i]);
    Serial.print(" ");
    arr[i] = 0;
  }
  Serial.println();
}
