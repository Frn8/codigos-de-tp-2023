#define N_MESES 12

const String TITULO_MES = "Mes";
const String TITULO_INGRESO = "Ingreso";
const String TEXTO_MAYOR_MES = "**Mes de mayor ingreso**";
const String TEXTO_MENOR_MES = "**Mes de menor ingreso**";
const String TEXTO_PROMEDIO = "**Promedio**";
const String TEXTO_TOTAL = "**Total**";

const String MESES[] = {
  "Enero",
  "Febrero",
  "Marzo",
  "Abril",
  "Mayo",
  "Junio",
  "Julio",
  "Agosto",
  "Septiembre",
  "Octubre",
  "Nobiembre",
  "Diciembre"
};

int maxStringLength(float valores[], unsigned int cantidadMeses, bool esMes, String extras[], unsigned int lengthExtras) {
  int lengthMax = 0;
  for (int i = 0; i < cantidadMeses; i++) {
    unsigned int valorLength;
    if (esMes) {
      valorLength = String(MESES[i]).length();
    } else {
      valorLength = String("$" + String(valores[i], 2)).length();
    }
    if (valorLength > lengthMax) {
      lengthMax = valorLength;
    }
  }


  for (int i = 0; i < lengthExtras; i++) {
    int valorLength = extras[i].length() + 1;
    if (valorLength > lengthMax) {
      lengthMax = valorLength;
    }
  }
  return lengthMax + 1;
}

int mayorMes(float valores[], unsigned int cantidadMeses) {
  if (cantidadMeses <= 0) {
    return -1;
  }
  float maxEncontrado = valores[0];
  int mesMasAlto = 0;
  for (int i = 1; i < cantidadMeses; i++) {
    if (valores[i] > maxEncontrado) {
      maxEncontrado = valores[i];
      mesMasAlto = i;
    }
  }
  return mesMasAlto;
}

int menorMes(float valores[], unsigned int cantidadMeses) {
  if (cantidadMeses <= 0) {
    return -1;
  }
  float minEncontrado = valores[0];
  int mesMasBajo = 0;
  for (int i = 1; i < cantidadMeses; i++) {
    if (valores[i] < minEncontrado) {
      minEncontrado = valores[i];
      mesMasBajo = i;
    }
  }
  return mesMasBajo;
}

float pedirIngreso(uint8_t mes) {
  Serial.print("Ingresos de ");
  Serial.print(MESES[mes - 1]);
  Serial.print(": $");
  while (!Serial.available()) {
    delay(2);
  }
  String entrada = Serial.readStringUntil('\n');  // Esto es para sacar la linea nueva
  entrada.replace(',', '.');                      // Cambiar la , por el punto
  float valor = entrada.toFloat();
  Serial.println(entrada);
  return valor;
}

void printSeparador(int maxLength) {
  Serial.print("|");
  for (int i = 0; i < maxLength; i++) {
    Serial.print("-");
  }
}

void printCelda(String contenido, int maxLength) {
  Serial.print("|");
  Serial.print(contenido);
  if (maxLength <= contenido.length() - 1) {
    return;
  }
  for (int i = 0; i < maxLength - contenido.length(); i++) {
    Serial.print(" ");
  }
}

void printTablaIngresos(float ingresos[N_MESES]) {
  int mesMasAlto = mayorMes(ingresos, N_MESES);
  int mesMasBajo = menorMes(ingresos, N_MESES);
  String mayorMesTexto = MESES[mesMasAlto] + " ($" + String(ingresos[mesMasAlto], 2) + ")";
  String menorMesTexto = MESES[mesMasBajo] + " ($" + String(ingresos[mesMasBajo], 2) + ")";

  String extrasMes[] = { TITULO_MES, TEXTO_PROMEDIO, TEXTO_TOTAL, TEXTO_MAYOR_MES, TEXTO_MENOR_MES };
  int maxMesLength = maxStringLength(ingresos, N_MESES, true, extrasMes, 5);
  String extrasIngresos[] = { TITULO_INGRESO, mayorMesTexto, menorMesTexto };
  int maxIngresosLength = maxStringLength(ingresos, N_MESES, false, extrasIngresos, 3);

  printCelda(TITULO_MES, maxMesLength);
  printCelda(TITULO_INGRESO, maxIngresosLength);
  Serial.println("|");
  printSeparador(maxMesLength);
  printSeparador(maxIngresosLength);
  Serial.println("|");

  float total = 0;
  for (int i = 0; i < N_MESES; i++) {
    total += ingresos[i];
    printCelda(MESES[i], maxMesLength);
    printCelda("$" + String(ingresos[i], 2), maxIngresosLength);
    Serial.println("|");
  }

  printCelda(TEXTO_MAYOR_MES, maxMesLength);
  printCelda(mayorMesTexto, maxIngresosLength);
  Serial.println("|");
  
  printCelda(TEXTO_MENOR_MES, maxMesLength);
  printCelda(menorMesTexto, maxIngresosLength);
  Serial.println("|");

  printCelda(TEXTO_PROMEDIO, maxMesLength);
  printCelda("$" + String(total / N_MESES, 2), maxIngresosLength);
  Serial.println("|");

  printCelda(TEXTO_TOTAL, maxMesLength);
  printCelda("$" + String(total, 2), maxIngresosLength);
  Serial.println("|");
}

void setup() {
  Serial.begin(115200);
  while (!Serial) {}
  delay(200);
  Serial.println("Hola!");
  delay(300);
}

void loop() {
  float ingresos[N_MESES];
  for (int i = 0; i < N_MESES; i++) {
    ingresos[i] = pedirIngreso(i + 1);
  }
  Serial.println("Resumen:");
  printTablaIngresos(ingresos);
}
