void toUpper(char* vector, int cant_elem) {
  for (size_t i = 0; i < cant_elem; i++) {
    char letra = vector[i];
    if (letra >= 'a' && letra <= 'z') {
      vector[i] = letra - 32;
    }
  }
}

void setup() {
  Serial.begin(115200);
}

void loop() {
  if (Serial.available() > 0) {
    String texto = Serial.readStringUntil('\n');
    // También podría hacer texto.toUpperCase() pero tengo que hacer esto
    // por que lo pide la consigna. Además devuelve un const char* por alguna razon.
    // No se si strdup es tan malo como malloc o memcpy pero... 😬
    // Así que automáticamente después de usarlo le hago free para evitar problemas.
    char* texto_arr = strdup(texto.c_str());  // Ni si quiera es estándar C, es POSIX... 😬😬😬
    toUpper(texto_arr, texto.length());
    Serial.println(texto_arr);
    // Esto es para evitar un memory leak del strdup más arriba
    free(texto_arr);  //! NO BORRAR!!!!!!!!!!!!!
  }
}
