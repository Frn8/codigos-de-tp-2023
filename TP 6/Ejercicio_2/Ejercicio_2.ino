#include <TimerOne.h>

#define BTN_SERVIR 2
#define LED_SIRVIENDO LED_BUILTIN
#define SENSOR_TAZA 3

int16_t tiempoRestante = 0;

enum SeleccionCafe {
  Corto = 0,
  Largo = 1,
  Nada = -1,
};

enum Estado {
  Eligiendo,
  Esperando_Boton,
  Sirviendo,
  Listo,
  Esperando_Taza,
} estado;

void servirTaza() {
  tiempoRestante--;
}

void setup() {
  pinMode(BTN_SERVIR, INPUT_PULLUP);
  pinMode(LED_SIRVIENDO, OUTPUT);
  pinMode(SENSOR_TAZA, INPUT_PULLUP);

  Timer1.initialize(1000);
  Timer1.attachInterrupt(servirTaza);
  Timer1.stop();

  Serial.begin(115200);
  Serial.setTimeout(100);
}

SeleccionCafe seleccionCafe = SeleccionCafe::Nada;

void loop() {
  switch (estado) {
    case Eligiendo:
      if (seleccionCafe == SeleccionCafe::Nada) {
        Serial.println("Elige el tipo de cafe: Corto o Largo");
        while (Serial.available() == 0) {}
        String seleccion = Serial.readStringUntil('\n');
        seleccion.toLowerCase();
        if (seleccion == "corto") {
          Serial.println("Seleccionó corto.");
          seleccionCafe = SeleccionCafe::Corto;
          estado = Estado::Esperando_Boton;
          tiempoRestante = 3000;
          Serial.println("Ponga su taza y presione el botón para servir.");
          break;
        } else if (seleccion == "largo") {
          Serial.println("Seleccionó largo");
          seleccionCafe = SeleccionCafe::Largo;
          estado = Estado::Esperando_Boton;
          tiempoRestante = 5000;
          Serial.println("Ponga su taza y presione el botón para servir.");
          break;
        }
        Serial.println("No se pudo entender que pedís con \"" + seleccion + "\"");
      } else {
        seleccionCafe = SeleccionCafe::Nada;
      }
      break;

    case Esperando_Boton:
      if (digitalRead(BTN_SERVIR) == HIGH && digitalRead(SENSOR_TAZA) == HIGH) {
        estado = Estado::Sirviendo;
        Serial.println("Sirviendo, por favor espere...");
        Timer1.start();
      }
      break;

    case Sirviendo:
      digitalWrite(LED_BUILTIN, HIGH);
      if (tiempoRestante <= 0) {
        Timer1.stop();
        digitalWrite(LED_BUILTIN, LOW);
        estado = Estado::Listo;
      }
      break;

    case Listo:
      Serial.println("Listo! Por favor retire su café.");
      estado = Estado::Esperando_Taza;
      break;

    case Esperando_Taza:
      if (digitalRead(SENSOR_TAZA) == LOW) {
        estado = Estado::Eligiendo;
      }
      break;
  }
}
