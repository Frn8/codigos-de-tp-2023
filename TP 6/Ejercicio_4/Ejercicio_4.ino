#include <TimerOne.h>

#define LED LED_BUILTIN
#define BTN 2
#define SPK 3

constexpr int16_t TIEMPO_CAMBIO_1 = 2000;
constexpr int16_t TIEMPO_CAMBIO_2 = 10000;

int16_t tiempoRestante = 0;

void restarTiempo() {
  tiempoRestante--;
}

void setup() {
  pinMode(LED, OUTPUT);
  pinMode(SPK, OUTPUT);
  pinMode(BTN, INPUT_PULLUP);

  Timer1.initialize(1000);
  Timer1.attachInterrupt(restarTiempo);
  Serial.begin(115200);
}

enum Estado {
  LedOff,
  Encendiendo,
  LedOn,
  Apagando
} estado;

void loop() {
  switch (estado) {
    case LedOff:
      Serial.println("LedOff");
      digitalWrite(LED, LOW);
      noTone(SPK);
      if (digitalRead(BTN) == LOW) {
        noInterrupts();
        estado = Estado::Encendiendo;
        tiempoRestante = TIEMPO_CAMBIO_1;
        interrupts();
      }
      break;
    case Encendiendo:
      Serial.print("Encendiendo ");
      Serial.println(tiempoRestante);
      noTone(SPK);
      if (digitalRead(BTN) == HIGH) {
        estado = Estado::LedOff;
      } else if (tiempoRestante <= 0) {
        estado = Estado::LedOn;
        noInterrupts();
        tiempoRestante = TIEMPO_CAMBIO_2;
        interrupts();
      }
      break;
    case LedOn:
      Serial.print("LedOn ");
      Serial.println(tiempoRestante);
      digitalWrite(LED, (uint16_t)tiempoRestante % 1000 > 500 ? HIGH : LOW);
      tone(SPK, 440);
      if (tiempoRestante <= 0) {
        estado = Estado::LedOff;
      } else if (digitalRead(BTN) == LOW) {
        noInterrupts();
        estado = Estado::Apagando;
        tiempoRestante = TIEMPO_CAMBIO_1;
        interrupts();
      }
      break;
    case Apagando:
      Serial.print("Apagando ");
      Serial.println(tiempoRestante);
      noTone(SPK);
      noInterrupts();
      if (digitalRead(BTN) == HIGH) {
        estado = Estado::LedOn;
        noInterrupts();
        tiempoRestante = TIEMPO_CAMBIO_2;
        interrupts();
      } else if (tiempoRestante <= 0) {
        estado = Estado::LedOff;
      }
      interrupts();
      break;
  }
}
