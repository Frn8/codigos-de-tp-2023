#include <TimerOne.h>

#define BOT_START 2
#define BOT_PAUSA 2

#define BOT_RESET 3

unsigned long milsTotales = 0;

void sumar1Ms() {
  milsTotales++;
}

struct Tiempo : public Printable {
  unsigned long segundos;
  unsigned long minutos;

  // Permite hacer print con este struct
  size_t printTo(Print& p) const {
    size_t r = 0;
    char segsStr[3];
    sprintf(segsStr, "%02lu", segundos);  // Agregar 0s para llenar 2 caracteres
    r += p.print(minutos);
    r += p.print(":");
    r += p.print(segsStr);
    return r;
  }
};

struct Tiempo msTotalesATiempo(unsigned long ms) {
  struct Tiempo res;
  unsigned long segsTotales = ms / 1000;

  res.segundos = segsTotales % 60;
  res.minutos = segsTotales / 60;

  return res;
}

enum Estado {
  Funcionando,
  Esperando,
  Parado,
} estado,
  siguienteEstado;

void esperarBoton(uint8_t pin, bool buscado, enum Estado siguiente);

uint8_t pinEsperado = NOT_A_PIN;
bool estadoPinEsperando = false;

void setup() {
  pinMode(BOT_START, INPUT_PULLUP);
  pinMode(BOT_PAUSA, INPUT_PULLUP);
  pinMode(BOT_RESET, INPUT_PULLUP);

  Serial.begin(115200);

  // Hacer que corra cada 1000 us (1 ms)
  Timer1.initialize(1000);
  Timer1.attachInterrupt(sumar1Ms);
}

void esperarBoton(uint8_t pin, bool buscado, enum Estado siguiente) {
  Serial.print("Cambiando a ");
  Serial.println(siguiente);
  estado = Estado::Esperando;
  siguienteEstado = siguiente;
  pinEsperado = pin;
  estadoPinEsperando = buscado;
}

void start() {
  Timer1.start();
  Serial.println("Temporizador reanudado");
  esperarBoton(BOT_START, LOW, Estado::Funcionando);
}

void pause() {
  Serial.println("Pausa");

  Timer1.stop();
  Serial.println("Temporizador pausado");
  esperarBoton(BOT_PAUSA, LOW, Estado::Parado);
}

void reset() {
  Serial.println("Reset");
  Timer1.stop();
  milsTotales = 0;
  Serial.println("Temporizador reiniciado");
  esperarBoton(BOT_RESET, LOW, Estado::Parado);
}

void loop() {
  switch (estado) {
    case Funcionando:
      if (digitalRead(BOT_PAUSA) == HIGH) {
        pause();
      } else if (digitalRead(BOT_RESET) == HIGH) {
        reset();
      }
      static unsigned long ultimosSecs = -1;
      struct Tiempo tiempo = msTotalesATiempo(milsTotales);
      if (tiempo.segundos != ultimosSecs) {
        Serial.println(tiempo);
        ultimosSecs = tiempo.segundos;
      }
      break;

    case Esperando:
      Serial.println("Esperando");
      if (digitalRead(pinEsperado) != estadoPinEsperando) {
        estado = siguienteEstado;
      }
      break;

    case Parado:
      if (digitalRead(BOT_RESET) == HIGH) {
        reset();
      } else if (digitalRead(BOT_START) == HIGH) {
        start();
      }
      break;
  }
}
