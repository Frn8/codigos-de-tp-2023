#include <TimerOne.h>

#include "antirebote.h"  // El ejercicio de botones antirebote

#define BTN_SUBIR 2
#define BTN_BAJAR 3
#define BTN_COMIENZO 4
#define MICROONDAS LED_BUILTIN

int32_t tiempo = 10000;

struct Tiempo : public Printable {
  unsigned long segundos;
  unsigned long minutos;

  // Permite hacer print con este struct
  size_t printTo(Print& p) const {
    size_t r = 0;
    char segsStr[3];
    sprintf(segsStr, "%02lu", segundos);  // Agregar 0s para llenar 2 caracteres
    r += p.print(minutos);
    r += p.print(":");
    r += p.print(segsStr);
    return r;
  }
};

struct Tiempo msTotalesATiempo(unsigned long ms) {
  struct Tiempo res;
  unsigned long segsTotales = ms / 1000;

  res.segundos = segsTotales % 60;
  res.minutos = segsTotales / 60;

  return res;
}

void restarTiempo() {
  tiempo--;
}

// Índices de los botones antirebote
size_t b_subir = agregarBoton(BTN_SUBIR, true);
size_t b_bajar = agregarBoton(BTN_BAJAR, true);
size_t b_comienzo = agregarBoton(BTN_COMIENZO, true);

void setup() {
  pinMode(MICROONDAS, OUTPUT);
  pinMode(BTN_SUBIR, INPUT_PULLUP);
  pinMode(BTN_BAJAR, INPUT_PULLUP);
  pinMode(BTN_COMIENZO, INPUT_PULLUP);

  Timer1.initialize(1000);
  Timer1.attachInterrupt(restarTiempo);
  Timer1.stop();
  Serial.begin(115200);
}

enum Estado {
  Encendido,
  Apagado
} estado;

void loop() {
  actualizarTodosLosBotones();
  static int32_t ultimoTiempo = 0;
  if (ultimoTiempo / 1000 != tiempo / 1000) {
    Serial.println(msTotalesATiempo(tiempo));
    ultimoTiempo = tiempo;
  }
  switch (estado) {
    case Apagado:
      if (botones[b_subir]->valor) {
        tiempo += 5000;
      } else if (botones[b_bajar]->valor) {
        tiempo -= 5000;
        if (tiempo < 0) {
          tiempo = 0;
        }
      } else if (botones[b_comienzo]->valor) {
        estado = Estado::Encendido;
        Timer1.start();
      }
      break;

    case Encendido:
      if (tiempo <= 0) {
        tiempo = 0;
        Timer1.stop();
        estado = Estado::Apagado;
      }
      if (botones[b_comienzo]->valor) {
        estado = Estado::Apagado;
        Timer1.stop();
      }
      break;
  }
}
