#define TEMP1 A0

// Temp mínima del sensor
#define MIN_TEMP -10000
// Temp máxima del sensor
#define MAX_TEMP 50000

// La tensión del micro
#define MAX_TENSION 5.0

constexpr uint16_t MAX_TENSION_MV = (uint16_t)MAX_TENSION * 1000;

// La resolución del ADC del micro en bits
#define RES_ADC 10

// El valor máximo del ADC del micro
constexpr uint16_t MAX_ADC = (2 << (RES_ADC - 1)) - 1;

// Leer el valor de un pin analógico y convertirlo a mV aprox.
inline uint16_t analogReadMV(uint8_t pin) {
  return map(analogRead(pin), 0, MAX_ADC, 0, MAX_TENSION_MV);
}

// La temperatura en milligrados celsius
int32_t temperatura = 0;

// El voltaje medido en millivolt
uint16_t voltaje = 0;

enum EstadoSensor
{
  Bajo = 10000,
  Medio = 25000,
  Alto = MAX_TEMP
} estado,
    ultimoEstado;

inline void setupSensor()
{
  pinMode(TEMP1, INPUT);
}

inline void actualizarEstado() {
  if (temperatura <= EstadoSensor::Bajo)
  {
    estado = EstadoSensor::Bajo;
  }
  else if (temperatura <= EstadoSensor::Medio)
  {
    estado = EstadoSensor::Medio;
  }
  else
  {
    estado = EstadoSensor::Alto;
  }
}

inline void actualizarSensor()
{
  ultimoEstado = estado;
  voltaje = analogReadMV(TEMP1);
  temperatura = map((int32_t)voltaje, 0, (int32_t)MAX_TENSION_MV, MIN_TEMP, MAX_TEMP);
}

constexpr uint16_t DEFAULT_VELOCIDAD_TEXTO = 25U;

uint16_t velocidad_texto = DEFAULT_VELOCIDAD_TEXTO;

void printConAnimacion(String textoStr)
{
  bool ultimoEsUnicode = false;
  const char *texto = textoStr.c_str();
  for (int i = 0; i < textoStr.length(); i++)
  {
    char caracter = texto[i];
    // Esto es todo para que funcione con caracteres unicode (como á, ñ, ý, 𐍈, 한, 💩, etc...).
    // Nota: No funciona con familias, parejas, etc. Tampoco tengo ganas de hacer caracteres truchos.
    // Esos son como 2 o 3 o cuantos quieras caracteres juntos por un "conector". (Excepto en banderas)
    if (bitRead(caracter, 7))
    { // Para cuando hay dos caracteres unicode pegados
      if (bitRead(caracter, 6) && ultimoEsUnicode == true)
      {
        delay(velocidad_texto);
      }
      ultimoEsUnicode = true;
    }
    else
    {
      ultimoEsUnicode = false;
    }
    Serial.write(caracter);
    if (bitRead(texto[i + 1], 7) == true)
    { // Si es un caracter unicode
      continue;
    }
    delay(velocidad_texto);
  }
}

inline void printlnConAnimacion(String texto)
{
  printConAnimacion(texto);
  Serial.println();
}

void setupSerial()
{
  Serial.begin(115200);

  // while (!Serial)
  // { // Esperar a que empiece Serial en un Arduino Leonardo, etc.
  //   delay(10);
  // }

  delay(400 + rand() % 300 - 150);

  printlnConAnimacion("Bienvenido a la interfaz de comando de Serial.");
  printlnConAnimacion("GentileSoft --- Estación metereológica --- Cargando...");
  delay(200 + rand() % 100 - 35);
  printlnConAnimacion("Listo!");
  delay(800);
}

// unsigned long ultimoPrint = millis();

// constexpr unsigned long MS_POR_PRINT = 500;

void loopSerial()
{
  if (estado != ultimoEstado)
  {
    switch (estado)
    {
    case Bajo:
      printConAnimacion("Temperatura baja 🥶🥶🥶");
      break;
    case Medio:
      printConAnimacion("Temperatura media 🙂🙂🙂");
      break;
    case Alto:
      printConAnimacion("Temperatura elevada 🥵🥵🥵");
      break;
    }
    // printConAnimacion(" (La temp. es ");
    // printConAnimacion(String((float)temperatura / 1000.0));
    // printlnConAnimacion(" ℃)");
  }
  // if (millis() - ultimoPrint > MS_POR_PRINT) {
  //   ultimoPrint = millis();
    printConAnimacion("La temperatura es ");
    printConAnimacion(String((float)temperatura / 1000.0));
    printlnConAnimacion(" ℃");
  // }
  ultimoEstado = estado;
}

inline void setupLed()
{
  pinMode(LED_BUILTIN, OUTPUT);
}

void loopLed()
{
  if (estado != ultimoEstado)
  {
    switch (estado)
    {
    case Alto:
      digitalWrite(LED_BUILTIN, HIGH);
      break;
    default:
      digitalWrite(LED_BUILTIN, LOW);
      break;
    }
  }
}

void setup()
{
  setupSerial();
  setupLed();
  setupSensor();
}

void loop()
{
  printConAnimacion("Sumando... ");
  int32_t sumaTemps = 0;
  for (size_t i = 0; i < 10; i++) {
    if (i != 0) {
      Serial.print(" ℃ + ");
    }
    actualizarSensor();
    sumaTemps += temperatura;
    Serial.print((float)temperatura / 1000.0);
    delay(100);
  }
  printConAnimacion(" ℃ = ");
  printConAnimacion(String((float)sumaTemps / 1000.0));
  printlnConAnimacion(" total");
  temperatura = sumaTemps / 10;
  actualizarEstado();
  loopLed();
  loopSerial(); // Esta tiene que ir ultima por como funciona
}
