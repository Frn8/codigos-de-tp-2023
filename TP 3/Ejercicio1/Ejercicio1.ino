#define POT1 A0
#define POT2 A1

#define LED1 8
#define LED2 7

// La tensión del micro
#define MAX_TENSION 5.0

// La resolución del ADC del micro en bits
#define RES_ADC 10

// Un map que funciona con floats y cualquier otro tipo numérico
constexpr float mapFloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

// El valor máximo del ADC del micro
constexpr uint16_t MAX_ADC = (2 << (RES_ADC - 1)) - 1;

inline float analogReadMapFloat(uint8_t pin, float salidaMin, float salidaMax) {
  return mapFloat(analogRead(pin), 0, MAX_ADC, salidaMin, salidaMax);
}

// Leer el valor de un pin analógico y convertirlo a V aprox.
inline float analogReadV(uint8_t pin) {
  return analogReadMapFloat(pin, 0, MAX_TENSION);
}

void setup() {
  Serial.begin(115200);

  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);

  pinMode(POT1, INPUT);
  pinMode(POT2, INPUT);
}

void loop() {
  float lectura1 = analogReadV(POT1);
  float lectura2 = analogReadV(POT2);

  if ((uint16_t)(lectura1 * 100) >= (uint16_t)(lectura2 * 100)) {
    digitalWrite(LED1, HIGH);
  } else {
    digitalWrite(LED1, LOW);
  }

  if ((uint16_t)(lectura2 * 100) >= (uint16_t)(lectura1 * 100)) {
    digitalWrite(LED2, HIGH);
  } else {
    digitalWrite(LED2, LOW);
  }

  Serial.print(lectura1);
  Serial.print("\t");
  Serial.println(lectura2);
  delay(50);
}
